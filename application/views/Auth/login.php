<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap Admin Theme v3</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>/assets/images/smanbul.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><center><a href="<?php echo base_url();?>">PUSAT DATA SMAN 1 CIBUNGBULANG</a></center></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			                <div class="social">
	                            <center>
	                            	<img src="<?php echo base_url();?>/assets/images/smanbul.png" style="width:180px;">
	                            </center>
	                            <div class="division">
	                                <hr class="left">
	                                <span>Login</span>
	                                <hr class="right">
	                            </div>
	                        </div>
			                <input class="form-control" type="text" placeholder="E-mail address">
			                <input class="form-control" type="password" placeholder="Password">
			                <div class="action">
			                    <input type="submit" value="LOGIN" class="btn btn-primary">
			                </div>                
			            </div>
			        </div>

			        <div class="already">
			            <p>Tidak Bisa Login ?  Segera hubungi </p>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  </body>
</html>